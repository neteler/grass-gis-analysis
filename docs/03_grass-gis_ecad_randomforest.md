# Classification with machine learning in GRASS GIS

To illustrate how to run machine learning methods in a GRASS GIS session we perform a climatic characterisation of Europe using the ECA&D data. The scope is to approximate a Koeppen-Geiger climate classification ([screenshot of global map](http://koeppen-geiger.vu-wien.ac.at/pics/map.jpg)) using ECA&D data. The Koeppen-Geiger climate classification is one of the most widely used climate classification systems.

## Method overview

1. extract 1200 random Koeppen-Geiger points in Europe (from Chen et al. 2013) describing the climate class at that point
2. sample further ECA&D variables at point positions and save to table
3. convert Koeppen-Geiger class names to numeric IDs
4. rasterize these sampling points
5. install and run r.learn.ml, with RandomForestClassifier
6. generate Koeppen-Geiger climatic raster map based on ECAD
7. verify the result

**References:**

* Chen, D. and H. W. Chen, 2013: Using the Köppen classification to quantify climate variation and change: An example for 1901-2010. Environmental Development, 6, 69-79, DOI: 10.1016/j.envdev.2013.03.007, http://hanschen.org/koppen/
* Kottek, M., J. Grieser, C. Beck, B. Rudolf, and F. Rubel, 2006: World Map of the Köppen-Geiger climate classification updated. Meteorol. Z., 15, 259-263, DOI: 10.1127/0941-2948/2006/0130, http://koeppen-geiger.vu-wien.ac.at/present.htm, http://koeppen-geiger.vu-wien.ac.at/pdf/Paper_2006.pdf


## Data download

* [CSV files for download](https://gitlab.com/neteler/grass-gis-geostat-2018/tree/master/intro/aux_data/)
    * --> 3 files:
        * koeppen_geiger_colors.csv
        * koeppen_geiger_legend_2017.csv
        * koppen_1901-2010.tsv

## Processing steps

![GRASS GIS logo](../img/grass.png)
For the steps, read on in [exercise_koeppen_geiger_ECAD_ML_classification.sh](exercise_koeppen_geiger_ECAD_ML_classification.sh).

## Results
The result obtained with RandomForest Classifier may look as follows:

<img src="../img/ecad_v17_koeppen_geiger_classification.png" alt="Koeppen-Geiger classification based on ECA&D data (1981-2010)" width="75%">
<br><i>Fig: Koeppen-Geiger classification based on ECA&D data (1981-2010)</i>

Compare your result to the map published in [Kottek, M. et al., 2006](http://www.schweizerbart.de/resources/downloads/paper_free/55034.pdf).


*Please read on in [04_grass-gis_ecad_regression.md](04_grass-gis_ecad_regression.md).*

